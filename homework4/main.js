'use strict'
class Hamburger {
	constructor(sizeVal,stuffingVal){
		try {
			if(!sizeVal){
				throw new Error('no size')
			}
			if(!stuffingVal){
				throw new Error('no stuffing')
			}
			for(let prop in Hamburger){
				if(Hamburger[prop]['name'] == sizeVal['name']){
					if(sizeVal['name'].indexOf('SIZE') == -1){
						throw new Error('invalid size: '+sizeVal['name'])
					}
				}
				
				if(Hamburger[prop]['name'] == stuffingVal['name']){
					if(stuffingVal['name'].indexOf('STUFFING') == -1){
						throw new Error('invalid stuffing: '+stuffingVal['name'])
					}	
				}
			}
			if(arguments.length > 2){
				let outputArr = []
				for (let i = 2; i < arguments.length; i++) {
					outputArr.push(arguments[i])
				}
				throw new Error('Not expected parameters: '+ outputArr.join(', '))
			}
			else{
				let size = sizeVal
				let stuffing = stuffingVal
				let toppings = []
				Hamburger.prototype.getFullSize = function(){
					return size
				}
				Hamburger.prototype.getFullStuffing = function(){
					return stuffing
				}
				Hamburger.prototype.getFullToppings = function(){
					return toppings
				}

			}
		} catch(e) {
			console.log('HamburgerException:',e.message)
		}
	}

	//constants
	static get SIZE_SMALL(){
		const small = {
			name:'SIZE_SMALL',
			price:50,
			calories:20
		}
		return small
	}
	static get SIZE_LARGE(){
		const large = {
			name:'SIZE_LARGE',
			price:100,
			calories:40	
		}
		return large
	}
	static get STUFFING_CHEESE(){
		const cheese = {
			name:'STUFFING_CHEESE',
			price:10,
			calories:20
		}
		return cheese 
	}
	static get STUFFING_SALAD(){
		const salad = {
			name:'STUFFING_SALAD',
			price:20,
			calories:5
		}
		return salad 
	}
	static get STUFFING_POTATO(){
		const potato = {
			name:'STUFFING_POTATO',
			price:15,
			calories:10	
		}
		return potato 
	}
	static get TOPPING_MAYO(){
		const mayo = {
			name:'TOPPING_MAYO',
			price:20,
			calories:5
		}
		return mayo 
	}
	static get TOPPING_SPICE(){
		const spice = {
			name:'TOPPING_SPICE',
			price:15,
			calories:0	
		}
		return spice 
	}

	//getters for private variables
	get getSize(){
		return this.getFullSize()['name']
	}
	get getStuffing(){
		return this.getFullStuffing()['name']
	}
	
	get getToppings(){
		let output = []
		for(let i = 0;i<this.getFullToppings().length;i++){
			output.push(this.getFullToppings()[i]['name'])
		}
		return output
	}


	//getters for private variables price
	get getSizePrice(){
		return this.getFullSize()['price']
	}
	get getStuffingPrice(){
		return this.getFullStuffing()['price']
	}
	get getToppingsPrice(){
		let result = 0;
		for(let i = 0;i<this.getFullToppings().length;i++){
			result+= this.getFullToppings()[i]['price']
		}
		return result
	}


	//getters for private variables calories
	get getSizeCalories(){
		return this.getFullSize()['calories']
	}
	get getStuffingCalories(){
		return this.getFullStuffing()['calories']
	}
	get getToppingsCalories(){
		let result = 0;
		for(let i = 0;i<this.getFullToppings().length;i++){
			result+= this.getFullToppings()[i]['calories']
		}
		return result
	}


	//Hamburger-methods
	addTopping(topping){
	try {
		for(let i=0;i<this.getFullToppings().length;i++){
			if(this.getFullToppings()[i]['name'] == topping['name']){
				throw new Error('duplicate topping: '+topping['name'])
			}
		}
		if(topping === undefined){
			throw new Error('invalid topping(doesn"t exist)')
		}
		else if(topping['name'].indexOf('TOPPING') == -1){
			throw new Error('invalid topping: '+topping['name'])
		}
		else{
			this.getFullToppings().push(topping)
		}
		}catch(e) {
			console.log('HamburgerException:',e.message);
		}
	}
	removeTopping(topping){
	try {
		if(this.getFullToppings().length == 0){
			throw new Error('no toppings to remove')
		}
		else if(topping === undefined){
			throw new Error('invalid topping')
		}
		else if(this.getToppings.indexOf(topping['name']) == -1){
			throw new Error('invalid topping: ' + topping['name'])
		}
		else if(topping['name'].indexOf('TOPPING') == -1){
			throw new Error('invalid topping: '+topping['name'])
		}
		else{
			for(let i = 0;i<this.getFullToppings().length;i++){
				if(this.getFullToppings()[i]['name'] == topping['name']){
					this.getFullToppings().splice(i,i+1)
				}
			}
		}
		}catch(e) {
			console.log('HamburgerException:',e.message);
		}	
	}


	calculatePrice(){
		return this.getSizePrice + this.getStuffingPrice + this.getToppingsPrice + ' grn';
	}

	calculateCalories(){
		return this.getSizeCalories + this.getStuffingCalories + this.getToppingsCalories + ' kcal'
	}
	
}
	



let obj = new Hamburger(Hamburger.SIZE_SMALL,Hamburger.STUFFING_POTATO)
console.log(obj)	
console.log(obj.getSize)	
console.log(obj.calculatePrice())
console.log(obj.calculateCalories())
console.log(obj.getStuffing)	
console.log(obj.getToppings)
obj.addTopping(Hamburger.TOPPING_MAYO)
obj.addTopping(Hamburger.TOPPING_SPICE)
console.log(obj.getToppings)
obj.removeTopping(Hamburger.TOPPING_SPICE)
console.log(obj.getToppings)
console.log(obj.calculatePrice())
console.log(obj.calculateCalories())

console.log('')
console.log('')
console.log('')
console.log('')
console.log('')
console.log('')


let obj2 = new Hamburger(Hamburger.SIZE_LARGE,Hamburger.STUFFING_CHEESE)
console.log(obj2)	
console.log(obj2.getSize)	
console.log(obj2.getStuffing)
console.log(obj2.calculatePrice())
console.log(obj2.calculateCalories())
	
obj2.addTopping(Hamburger.TOPPING_MAYO)
obj2.addTopping(Hamburger.TOPPING_SPICE)
console.log(obj2.getToppings)
obj.removeTopping(Hamburger.TOPPING_MAYO)
console.log(obj2.getToppings)
console.log(obj2.calculatePrice())
console.log(obj2.calculateCalories())