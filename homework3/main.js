'use strict';
function Hamburger(size, stuffing){
	var toppings = [];
	try {
		if(!size){
			throw new Error('no size')
		}
		if(!stuffing){
			throw new Error('no stuffing')
		}
		for(var prop in Hamburger){
			if(Hamburger[prop]['name'] == size['name']){
				if(size['name'].indexOf('SIZE') == -1){
					throw new Error('invalid size: '+size['name'])
				}
			}
			
			if(Hamburger[prop]['name'] == stuffing['name']){
				if(stuffing['name'].indexOf('STUFFING') == -1){
					throw new Error('invalid stuffing: '+stuffing['name'])
				}	
			}
		}
		
		if(arguments.length > 2){
			var outputArr = []
			for (var i = 2; i < arguments.length; i++) {
				outputArr.push(arguments[i])
			}
			throw new Error('Not expected parameters: '+ outputArr.join(', '))
		}
		else{
			var size = size;
			var stuffing = stuffing;
		}
	} catch(e) {
		console.log('HamburgerException:',e.message);
	}
Hamburger.prototype.getSize = function (){
	return size['name']
}
Hamburger.prototype.getSizePrice = function (){
	return size['price']
}
Hamburger.prototype.getStuffingPrice = function (){
	return stuffing['price']
}

Hamburger.prototype.getToppings = function (){
	var output = []
	for(var i = 0;i<toppings.length;i++){
		output.push(toppings[i]['name'])
	}
	return output
}
Hamburger.prototype.getFullToppings = function (){
	return toppings
}

Hamburger.prototype.getToppingsPrice = function (){
	var result = 0;
	for(var i = 0;i<toppings.length;i++){
		result+= toppings[i]['price']
	}
	return result
}
Hamburger.prototype.getSizeCalories = function(){
	return size['calories']
}
Hamburger.prototype.getStuffingCalories = function(){
	return stuffing['calories']
}
Hamburger.prototype.getToppingsCalories = function(){
	var result = 0;
	for(var i = 0;i<toppings.length;i++){
		result+= toppings[i]['calories']
	}
	return result
}


Hamburger.prototype.getStuffing = function (){
	return stuffing['name']
}		
}

Hamburger.SIZE_SMALL ={
	name:'SIZE_SMALL',
	price:50,
	calories:20
} ;
Hamburger.SIZE_LARGE ={
	name:'SIZE_LARGE',
	price:100,
	calories:40	
} ;
Hamburger.STUFFING_CHEESE ={
	name:'STUFFING_CHEESE',
	price:10,
	calories:20
} ;
Hamburger.STUFFING_SALAD ={
	name:'STUFFING_SALAD',
	price:20,
	calories:5	
} ;
Hamburger.STUFFING_POTATO ={
	name:'STUFFING_POTATO',
	price:15,
	calories:10	
} ;
Hamburger.TOPPING_MAYO ={
	name:'TOPPING_MAYO',
	price:20,
	calories:5
} ;
Hamburger.TOPPING_SPICE ={			
	name:'TOPPING_SPICE',
	price:15,
	calories:0	
} ;



Hamburger.prototype.addTopping = function (topping){
	try {
		for(var i=0;i<this.getFullToppings().length;i++){
			if(this.getFullToppings()[i]['name'] == topping['name']){
				throw new Error('duplicate topping: '+topping['name'])
			}
		}
		if(topping === undefined){
			throw new Error('invalid topping(doesn"t exist)')
		}
		else if(topping['name'].indexOf('TOPPING') == -1){
			throw new Error('invalid topping: '+topping['name'])
		}
		else{
			this.getFullToppings().push(topping)
		}
		}catch(e) {
			console.log('HamburgerException:',e.message);
		}
}

Hamburger.prototype.removeTopping = function (topping){
	try {
		if(this.getFullToppings().length == 0){
			throw new Error('no toppings to remove')
		}
		else if(topping === undefined){
			throw new Error('invalid topping')
		}
		else if(this.getToppings().indexOf(topping['name']) == -1){
			throw new Error('invalid topping: ' + topping['name'])
		}
		else if(topping['name'].indexOf('TOPPING') == -1){
			throw new Error('invalid topping: '+topping['name'])
		}
		else{
			for(var i = 0;i<this.getFullToppings().length;i++){
				if(this.getFullToppings()[i] == topping){
					this.getFullToppings().splice(i,i+1)
				}
			}
		}
		}catch(e) {
			console.log('HamburgerException:',e.message);
		}	
}

Hamburger.prototype.calculatePrice = function (){
	return this.getSizePrice() + this.getStuffingPrice() + this.getToppingsPrice() + ' grn';
}

Hamburger.prototype.calculateCalories = function (){
	return this.getSizeCalories() + this.getStuffingCalories() + this.getToppingsCalories() + ' kcal'
}

var obj = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_POTATO)
console.log(obj)
obj.addTopping(Hamburger.TOPPING_MAYO);
console.log(obj.getToppings());
console.log(obj.calculatePrice());
console.log(obj.calculateCalories());
console.log(obj.getSize());
console.log(obj.getStuffing());


var obj2 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_SALAD)
obj2.addTopping(Hamburger.TOPPING_SPICE);
console.log(obj2.getToppings());
console.log(obj2.calculatePrice());
console.log(obj2.calculateCalories());
console.log(obj2.getSize());
console.log(obj2.getStuffing());
console.log(obj2.valueOf())